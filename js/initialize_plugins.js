// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		// $(function(){
		// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
		// 	ua = navigator.userAgent,

		// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

		// 	scaleFix = function () {
		// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
		// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
		// 			document.addEventListener("gesturestart", gestureStart, false);
		// 		}
		// 	};
			
		// 	scaleFix();
		// });
		// var ua=navigator.userAgent.toLocaleLowerCase(),
		//  regV = /ipod|ipad|iphone/gi,
		//  result = ua.match(regV),
		//  userScale="";
		// if(!result){
		//  userScale=",user-scalable=0"
		// }
		// document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var sticky = $(".sticky"),
			matchheight = $("[data-mh]"),
		    owl = $(".owl-carousel"),
		    carouselStockNews = $(".carousel_stock_news"),
		    carouselScore = $(".score_carousel"),
		    isotope = $(".isotope"),
		    isotope_main = $(".isotope_main"),
			popup = $("[data-popup]"),
			zoomSlider = $("#zoomslider"),
			fancybox = $(".fancybox"),
			windowW = $(window).width(),
			windowH = $(window).height();


			if(sticky.length){
					include("plugins/sticky.js");
					include("plugins/jquery.smoothscroll.js");
			}
			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(isotope.length || isotope_main.length){
					include('plugins/isotope/isotope.pkgd.js');
					include('plugins/isotope/imagesLoaded.js');
					include('plugins/isotope/packery-mode.pkgd.js');
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(owl.length || carouselStockNews.length || carouselScore.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}
			if(zoomSlider.length){
					include('plugins/elevatezoom-master/jquery.elevatezoom.js');
			}
			if(fancybox.length){
					include('plugins/fancybox/jquery.fancybox.js');
					include('plugins/jquery.easing.1.3.js');
			}


					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

			$.fn.imagesLoaded = function () {

		       var $imgs = this.find('img[src!=""]');

		       if (!$imgs.length) {return $.Deferred().resolve().promise();}

		       var dfds = [];

		       $imgs.each(function(){
		           var dfd = $.Deferred();
		           dfds.push(dfd);
		           var img = new Image();
		           img.onload = function(){dfd.resolve();}
		           img.onerror = function(){dfd.resolve();}
		           img.src = this.src;
		       });

		       return $.when.apply($,dfds);

		   }

		


		$(document).ready(function(){




			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */

					if (sticky.length){
						function bodyWidth(){
							var windowW = $(window).width();

							if(windowW >= 768 && !$('body').hasClass('sticky_menu')){
								sticky.sticky({
							        topspacing: 0,
							        styler: 'is-sticky',
							        animduration: 0,
							        unlockwidth: false,
							        screenlimit: false,
							        stopper: '#footer',
							        sticktype: 'alonemenu'
								});
								$('body').addClass('sticky_menu');
								$('body').find('.menu_box').addClass('sticky');
							}
							else if(windowW <= 767 && $('body').hasClass('sticky_menu')){
								$('body').find('.menu_box').removeClass('sticky is-sticky');
								$('body').find('.menu_box').attr('style', '');
								$('body').find('.placeholder').remove();
								$('body').removeClass('sticky_menu');
								$(window).off('scroll');
							}
						}

						bodyWidth();

						$(window).on('resize , scroll', function(){
							bodyWidth();
						});

					};

			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */




			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length || carouselStockNews.length || carouselScore.length){

						// owl.owlCarousel start
						owl.each(function(){
				  			var	$this = $(this),
				  			    mainSliderItem = $this.find(".main_slider_item").length;

				  			if(mainSliderItem > 1){
						  		$this.owlCarousel({
						  			singleItem : true,
									items : 1,
									loop: true,
									smartSpeed:1000,
									autoplay: true,
						    		autoplayTimeout: 5000,
									smartSpeed:2000,
									nav: true,
							        navigationText: [ '', '' ]
							  	});
				  			}

				  		});

						// carouselStockNews.owlCarousel start
						carouselStockNews.each(function(){
					    	var $this = $(this),
					      		items = $this.data('items');

					    	$this.owlCarousel({
					    		stagePadding: 0,
					    		autoplay: false,
					   //  		autoplayTimeout: 5000,
								// smartSpeed:2000,
								// autoplayHoverPause: true,
								items : 3,
								loop: false,
					    		nav: true,
					            navText: [ '', '' ],
					            margin: 22,
					            responsive : items
					    	});
					    });

						// carouselScore.owlCarousel start
					    carouselScore.each(function(){
					    	var $this = $(this),
					      		items = $this.data('items');

					    	$this.owlCarousel({
					    		stagePadding: 0,
					    		autoplay: true,
					    		autoplayTimeout: 5000,
								smartSpeed:2000,
								autoplayHoverPause: true,
								items : 5,
								loop: false,
					    		nav: true,
					            navText: [ '', '' ],
					            margin: 24,
					            responsive : items
					    	});
					    });


					}

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			FANCYBOX START
			------------------------------------------------ */

					if(fancybox.length){
				        fancybox.fancybox({
				        	'titleShow'     : true,
							'transitionIn'	: 'none',
							'transitionOut'	: 'none',
							'speedIn'		: 600, 
							'speedOut'		: 200, 
							'type'          : 'image',
							'changeFade'    : 0,
							'overlayShow'	: true,
							openEffect      : 'none',
						    closeEffect     : 'none',
						    openSpeed       : 150,
				            openEasing      : 'easeInOutQuad',
				            closeEasing     : 'easeInOutQuad',
				            // nextEasing      : 'easeOutQuint',
				            // prevEasing      : 'easeInQuint',
				        	// 'padding'		: 10,
						    // prevEffect      : 'fade',
						    // nextEffect      : 'fade',
				            helpers         : {

						    		title: {
						    			type : 'inside'
						    		}
						    		
					    	}
						});
					}

			/* ------------------------------------------------
			FANCYBOX END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal({
						    	beforeOpen: function(){
						    			/* ------------------------------------------------
										ZOOM-SLIDER START
										------------------------------------------------ */
										setTimeout(function(){

								    		if(zoomSlider.length){
												zoomSlider.elevateZoom({
													responsive : true,
													// constrainType:"height", 
													// constrainSize:274, 
													zoomType: "lens", 
													containLensZoom: true, 
													gallery:'gallery', 
													cursor: 'pointer', 
													galleryActiveClass: "active"
												});
								                zoomSlider.bind("click", function(e) { 
								                    var ez = $(zoomSlider).data('elevateZoom');   
								                    $.fancybox(ez.getGalleryList()); return false; 
								                });
								                
							                }
										}, 100)
					                	/* ------------------------------------------------
										ZOOM-SLIDER END
										------------------------------------------------ */
						    	},
						    	afterClose: function(){
									setTimeout(function(){
							    		if(zoomSlider.length){
			        						$('.zoomContainer').remove();
							    		}
						    		}, 100)
						    	}

						    });

						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */



		});


		$(window).load(function(){

			/* ------------------------------------------------
			ISOTOPE START
			------------------------------------------------ */

					if (isotope.length || isotope_main.length){
						// init Isotope
						isotope.isotope({
						 	itemSelector: ".grid_col",
							layoutMode: 'fitRows'
						});
						// layout Isotope after each image loads
						isotope.imagesLoaded().progress( function() {
						 	isotope.isotope('layout');
						});


						// init isotope_main
						isotope_main.isotope({
						 	itemSelector: ".grid_col_main",
							layoutMode: 'packery'
						});
						isotope_main.imagesLoaded().then(function(){
								isotope_main.isotope('layout');
						});

					}

			/* ------------------------------------------------
			ISOTOPE END
			------------------------------------------------ */

		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================


